package ru.chelnokov.tasks;

import java.util.Arrays;

/**
 * Класс, считающий сумму значений закрашенных четвертей матрицы
 *
 * @author Chelnokov E.I., 16IT18K
 */
public class Matrix {
    public static void main(String[] args) {
        int summ = 0;
        int[][] a = initMatrix();
        summ = summ(summ, a);

        matrixOutput(summ, a);
    }

    private static void matrixOutput(int summ, int[][] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(Arrays.toString(a[i]));
        }
        System.out.print("Сумма значений первой и третьей четверти матрицы равна: " + summ);
    }

    /**
     * инициализирует матрицу
     *
     * @return matrix[][] - готовая матрица
     */
    private static int[][] initMatrix() {
        int[][] matrix = new int[3][3];
        for (int i = 0; i < matrix.length; i++) {//столбцы
            for (int j = 0; j < matrix[i].length; j++) {//строки
                matrix[i][j] = 1 + (int) (Math.random() * 9);
            }
        }
        return matrix;
    }

    /**
     * Считает сумму четвертей матрицы
     *
     * @param summ сумматор для четвертей
     * @param a исходная матрица
     * @return сумму четвертей матрицы
     */
    private static int summ(int summ, int[][] a) {
        for (int i = 0; i < a.length; i++) {//столбцы
            for (int j = 0; j < a[i].length; j++) {//строки
                int annual = a.length / 2;
                if (a.length % 2 == 1) {
                    if ((i < annual) && (j > annual)) {
                        summ = summ + a[i][j];
                    }
                    if ((i > annual) && ((j < annual))) {
                        summ = summ + a[i][j];
                    }
                } else {
                    if ((i < annual) && (j >= annual)) {
                        summ = summ + a[i][j];
                    }
                    if ((i >= annual) && ((j < annual))) {
                        summ = summ + a[i][j];
                    }
                }
            }
        }
        return summ;
    }

}
