package ru.chelnokov.tasks;
import java.util.Arrays;

/**
 * Класс считает сумму чисел по правую сторону диагонали матрицы 5х5(без учета самой диагонали)
 *
 * @author Chelnokov E.I., 16It18K
 *
 */

public class Diagonal {
    public static void main(String[] args) {
        int lenght = 5;
        int sum = 0;
        int[][] matrix = new int[lenght][lenght];
        for (int i = 0; i < matrix.length; i++) {//столбцы
            for (int j = 0; j < matrix[i].length; j++) {//строки
                matrix[i][j] = 1 + (int) (Math.random() * 9);
            }
        }
        
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i; j < matrix[i].length; j++) {
                sum = sum + matrix[i][j];
            }
        }

        for (int[] aMatrix : matrix) {//построчно выводит матрицу
            System.out.println(Arrays.toString(aMatrix));
        }
        System.out.print("Сумма элементов матрицы по одну диагональ равна: " + sum);
    }
}
